# SpeechDatasets.jl

A Julia package to download and prepare speech corpus.

## Installation

Make sure to add the [PTAL registry](https://gitlab.lisn.upsaclay.fr/PTAL/Registry)
to your julia installation. Then, install the package as usual:
```julia
pkg> add SpeechDatasets
```
## Usage
```julia
dataset(:NAME, inputdir, outputdir; <keyword arguments>)
```
## Example

```julia
julia> using SpeechDatasets

julia> ds = dataset(:TIMIT, "/path/to/timit/dir", "outputdir"; subset="train")

# Access any element
julia> ds[5]

# Lexicons
julia> CMUDICT("outputfile")
...

julia> TIMITDICT("/path/to/timit/dir")
...

```

## Dataset paths

`test/runtests.jl` assumes the paths to the different datasets are stores in `DatasetsDocumentation/corpora.json` in the following format:

```
{
    "NAME": {
        "path": "path/to/corpus/root/
    },
}
```

LISN users should clone the internal repository `PTAL/Datasets/DatasetsDocumentation`.

## License

This software is provided under the [CeCILL-C license](https://cecill.info/licences.en.html) (see [`/license`](/license))

