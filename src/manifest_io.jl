# SPDX-License-Identifier: CECILL-C

#=====================================================================#
# JSON serialization of a manifest item

function Base.show(io::IO, m::MIME"application/json", s::AudioSources.FileAudioSource)
    compact = get(io, :compact, false)
    indent = get(io, :indent, 0)
    printfn = compact ? print : println
    printfn(io, "{")
    printfn(io, repeat(" ", indent + (compact ? 0 : 2)), "\"type\": \"path\", ")
    printfn(io, repeat(" ", indent + (compact ? 0 : 2)), "\"data\": \"", s.path, "\"")
    print(io, repeat(" ", indent), "}")
end

function Base.show(io::IO, m::MIME"application/json", s::AudioSources.URLAudioSource)
    compact = get(io, :compact, false)
    indent = get(io, :indent, 0)
    printfn = compact ? print : println
    printfn(io, repeat(" ", indent), "{")
    printfn(io, repeat(" ", indent + (compact ? 0 : 2)), "\"type\": \"url\", ")
    printfn(io, repeat(" ", indent + (compact ? 0 : 2)), "\"data\": \"", s.url, "\"")
    print(io, repeat(" ", indent), "}")
end

function Base.show(io::IO, m::MIME"application/json", s::AudioSources.CmdAudioSource)
    compact = get(io, :compact, false)
    indent = get(io, :indent, 0)
    printfn = compact ? print : println
    printfn(io, "{")
    printfn(io, repeat(" ", indent + (compact ? 0 : 2)), "\"type\": \"cmd\", ")
    strcmd = replace("$(s.cmd)", "`" => "")
    printfn(io, repeat(" ", indent + (compact ? 0 : 2)), "\"data\": \"$(strcmd)\"")
    print(io, repeat(" ", indent), "}")
end

function Base.show(io::IO, m::MIME"application/json", r::Recording)
    compact = get(io, :compact, false)
    indent = compact ? 0 : 2
    printfn = compact ? print : println
    printfn(io, "{")
    printfn(io, repeat(" ", indent), "\"id\": \"", r.id, "\", ")
    print(io, repeat(" ", indent), "\"src\": ")
    show(IOContext(io, :indent => compact ? 0 : 2), m, r.source)
    printfn(io, ", ")
    print(io, repeat(" ", indent), "\"channels\": [")
    for (i, c) in enumerate(r.channels)
        print(io, c)
        i < length(r.channels) && print(io, ",")
    end
    printfn(io, "], ")
    printfn(io, repeat(" ", indent), "\"samplerate\": ", r.samplerate)
    print(io, "}")
end

function Base.show(io::IO, m::MIME"application/json", a::Annotation)
    compact = get(io, :compact, false)
    indent = compact ? 0 : 2
    printfn = compact ? print : println
    printfn(io, "{")
    printfn(io, repeat(" ", indent), "\"id\": \"", a.id, "\", ")
    printfn(io, repeat(" ", indent), "\"recording_id\": \"", a.recording_id, "\", ")
    printfn(io, repeat(" ", indent), "\"start\": ", a.start, ", ")
    printfn(io, repeat(" ", indent), "\"duration\": ", a.duration, ", ")
    printfn(io, repeat(" ", indent), "\"channels\": ", a.channels |> json, ", ")
    printfn(io, repeat(" ", indent), "\"data\": ", a.data |> json)
    print(io, "}")
end

function JSON.json(r::Union{Recording, Annotation}; compact = true)
    out = IOBuffer()
    show(IOContext(out, :compact => compact), MIME("application/json"), r)
    String(take!(out))
end

#=====================================================================#
# Converting a dictionary to a manifest item.

function AudioSource(d::Dict)
    if d["type"] == "path"
        T = AudioSources.FileAudioSource
    elseif d["type"] == "url"
        T = AudioSources.URLAudioSource
    elseif d["type"] == "cmd"
        T = AudioSources.CmdAudioSource
    else
        throw(ArgumentError("invalid type: $(d["type"])"))
    end
    T(d["data"])
end

Recording(d::Dict) = Recording(
    d["id"],
    AudioSource(d["src"]),
    convert(Vector{Int}, d["channels"]),
    d["samplerate"]
)

Annotation(d::Dict) = Annotation(
    d["id"],
    d["recording_id"],
    d["start"],
    d["duration"],
    d["channels"],
    d["data"]
)

#=====================================================================#
# Writing / reading manifest from file.

function writemanifest(io::IO, manifest::Dict)
    writefn = x -> println(io, x)
    for item in values(manifest)
        item |> json |> writefn
    end
end

function readmanifest(io::IO, T)
    manifest = Dict{AbstractString, T}()
    for line in eachline(io)
        item = JSON.parse(line) |> T
        manifest[item.id] = item
    end
    manifest
end

# Some utilities
manifestname(::Type{<:Recording}, name) = "recordings.jsonl"
manifestname(::Type{<:Annotation}, name) = "annotations-$name.jsonl"

"""
    load_manifest(Annotation, path)
    load_manifest(Recording, path)

Load Recording/Annotation manifest from `path`.
"""
load_manifest(T::Type{<:Union{Recording, Annotation}}, path) = open(f -> readmanifest(f, T), path, "r")

function checkdir(dir::AbstractString)
    isdir(dir) || throw(ArgumentError("$dir is not an existing directory"))
end
