"""
    struct DatasetBuilder{name}
Allow to dispatch main dataset functions (`download()`, `prepare()`).
# Parameter
- `name` Dataset identifier
# Fields
- `kwargs::NamedTuple` Keyword arguments supported by the dataset associated to `name`
"""
struct DatasetBuilder{name}
    kwargs::NamedTuple
end

"""
    DatasetBuilder(name::Symbol)
Construct a DatasetBuilder for a given name.
Implementations for each name are done by calling [`declareBuilder(name)`](@ref) (automatically done for each supported name).
"""
DatasetBuilder(name::Symbol) = DatasetBuilder{name}()

"""
    get_kwargs(func_name::Function, args_types::Tuple)
Return a `NamedTuple` containing each supported kwarg and its default value for a given method.
# Arguments
- `func_name` is the name of the function
- `args_types` is a tuple of argument types for the desired method
"""
function get_kwargs(func_name::Function, args_types::Tuple)
    kwargs_names = Base.kwarg_decl(methods(func_name, args_types)[1])
    l = length(kwargs_names)
    if l==0
        # no kwargs
        return (;)
    else
        # lowered form of the method contains kwargs default values, but some are hidden
        code = code_lowered(func_name, args_types)[1].code
        str_code = ["$c" for c in code]
        # get index corresponding to the function
        index = findall(x -> occursin("$func_name", x), str_code)[1]
        # get lowered value of each kwarg
        values = code[index].args[2:2+l-1]
        # get back the original value according to the lowered value type
        kwargs_values = map(v ->
            if v isa Core.SSAValue
                eval(code[v.id])
            elseif v isa GlobalRef
                eval(v)
            else
                v
            end
            , values)
        # reconstruct kwargs
        NamedTuple(zip(kwargs_names, kwargs_values))
    end
end

"""
    get_nametype(name::String)
Return a symbol corresponding to the name. This symbol is used to identify the dataset.
"""
get_nametype(name::String) = Symbol(replace(name, " "=>"")) # simply remove space

"""
    get_dataset_kwargs(name::String)
Return a `NamedTuple` containing each supported kwarg and its default value for a dataset identified by name.
"""
get_dataset_kwargs(name::String) = get_dataset_kwargs(get_nametype(name))
"""
    get_dataset_kwargs(name::Symbol)
Return a `NamedTuple` containing each supported kwarg and its default value for a dataset identified by symbol (nametype).
"""
get_dataset_kwargs(name::Symbol) = get_kwargs(prepare, (DatasetBuilder{name}, AbstractString, AbstractString))


"""
    declareBuilder(name::Symbol)
Declare a functor for a DatasetBuilder of type `name`.\n
A `DatasetBuilder{name}` object can now be created, and will hold the supported kwargs for the corresponding dataset.
"""
function declareBuilder(name::Symbol)
    kwargs = get_dataset_kwargs(name)
    quotedname = Meta.quot(name)
    eval(Meta.parse("(::Type{DatasetBuilder{$quotedname}})() = DatasetBuilder{$quotedname}($kwargs)"))
end

# Each dataset should implement prepare() (and optionnaly download()) to be usable in dataset()
"""
    Base.download(builder::DatasetBuilder{name}, dir::AbstractString)
Download the dataset identified by `name` into `dir`.\n
Each dataset has its own implementation if download is supported (for example, a proprietary dataset might not implements download).
"""
Base.download

"""
    prepare(::DatasetBuilder{name}, inputdir, outputdir; <keyword arguments>)
Create manifest files into `outputdir` from dataset in `inputdir`. \n
Each dataset has its own implementation, and can have optional keyword arguments, they can be accessed with [`get_dataset_kwargs(name::String)`](@ref).\n
Implementing this function is mandatory for a dataset to be compatible with `dataset()`
"""
function prepare end

