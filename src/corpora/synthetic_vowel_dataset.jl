# SPDX-License-Identifier: CECILL-B

#######################################################################

function prepare(::Val{:SYNTHETIC_VOWEL_DATASET}, synsetdir, odir)
    # Validate the data directory
    ! isdir(synsetdir) && throw(ArgumentError("invalid path $(synsetdir)"))

    # Create the output directory.
    dir = mkpath(odir)
    rm(joinpath(odir, "recordings.jsonl"), force=true)

    ## Recordings
    @info "Extracting recordings from $synsetdir"
    recordings = synset_recordings(synsetdir)

    manifestpath = joinpath(odir, "recordings.jsonl")
    @info "Creating $manifestpath"
    open(manifestpath, "a") do f
        writemanifest(f, recordings)
    end

    # Metadata
    @info "Extracting metadata from $synsetdir/$(basename(synsetdir))_detailed.csv"
    metadata = synset_metadata(synsetdir)

    manifestpath = joinpath(odir, "annotations.jsonl")
    @info "Creating $manifestpath"
    open(manifestpath, "w") do f
        writemanifest(f, metadata)
    end

end


function synset_recordings(dir::AbstractString)
    ! isdir(dir) && throw(ArgumentError("expected directory $dir"))

    recordings = Dict()
    for (root, subdirs, files) in walkdir(dir)
        for file in files
            lname, ext = splitext(file)
            sname = split(lname, "_")

            ext != ".wav" && continue

            spkid = sname[3]
            gender, vowel, sigid = sname[4], sname[5], sname[6]

            path = joinpath(root, file)

            id = "spk_$(spkid)_$(gender)_$(vowel)_$(sigid)"

            audio_src = AudioSources.FileAudioSource(path)

            recordings[id] = Recording(
                id,
                audio_src;
                channels = [1],
                samplerate = 16000
            )
        end
    end
    recordings
end


function synset_metadata(dir)
    ! isdir(dir) && throw(ArgumentError("expected directory $dir"))

    metadata = Dict()

    fpath = joinpath("$dir", "$(basename(dir))_detailed.csv")
    df = DataFrame(CSV.File(fpath))

    # Get number of filter coefficients
    countfilter = count(col -> occursin(r"^a_\d+$", col), names(df))

    for row in eachrow(df)

        # Get metadata
        spk = split(row["fname"], "_")
        spkid = spk[3] 
        gender = row["gender"]
        vowel = row["vowel"]
        sigid = split(row["signal"], "_")[end]
        f₀ = row["f0"]
        ϕ = row["ϕ"]
        vtl = row["vtl"]
        filter = Dict(["a_$i" => row["a_$i"] for i in 1:countfilter])
        angles = Dict(["θ$i" => row["θ$i"] for i in 1:Int(countfilter/2)])
        magnitudes = Dict(["r$i" => row["r$i"] for i in 1:Int(countfilter/2)])

        id = "spk_$(spkid)_$(gender)_$(vowel)_$(sigid)"
        metadata[id] = Annotation(
            id,  # recording id and annotation id are the same since we have
            id,  # a one-to-one mapping
            -1,  # start and duration is -1 means that we take the whole
            -1,  # recording
            [1], # only 1 channel (mono recording)
            Dict(
                    "spk" => spkid,
                    "gender" => gender,
                    "vowel" => vowel,
                    "sigid" => sigid,
                    "f₀" => f₀,
                    "ϕ" => ϕ,
                    "vtl" => vtl,
                    "filter" => filter,
                    "angles" => angles,
                    "magnitudes" => magnitudes
            )
        )
    end
    metadata
end

