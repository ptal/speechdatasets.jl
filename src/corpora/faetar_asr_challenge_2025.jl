
function prepare(::Val{:FAETAR_ASR_CHALLENGE_2025}, data_dir, output_dir; subsets=["train", "dev", "test", "unlab"])
    # Validate the data directory
    !isdir(data_dir) && throw(ArgumentError("invalid path $(data_dir)"))

    # Create the output directory.
    output_dir = mkpath(output_dir)
    rm(joinpath(output_dir, "recordings.jsonl"), force=true)
    rm(joinpath(output_dir, "annotations.jsonl"), force=true)

    annotations = Dict()
    recordings = Dict()

    for subset in subsets
        flist = readdir(joinpath(data_dir, subset))

        # For each file in the subset
        for file in filter(s -> endswith(s, ".wav"), flist)

            id = strip(file, ['.', 'w', 'a', 'v'])

            # Get audio source
            audio_src = AudioSources.FileAudioSource(joinpath(data_dir, subset, file))

            recordings[id] = Recording(
                id,
                audio_src;
                channels=[1],
                samplerate=1600
            )

            # Get transcription if file is in train or dev, else leave empty
            if subset ∈ ["train", "dev"]
                transcription_file = string(id, ".txt")
                transcription = read(joinpath(data_dir, subset, transcription_file), String)
                transcription = strip(transcription, '\n')

                annotations[id] = Annotation(
                    id,
                    id,
                    -1,
                    -1,
                    [1],
                    Dict(
                        "transcription" => transcription,
                        "subset" => subset)
                )

            else
                annotations[id] = Annotation(
                    id,
                    id,
                    -1,
                    -1,
                    [1],
                    Dict(
                        "subset" => subset)
                )
            end


        end

    end


    manifestpath = joinpath(output_dir, "recordings.jsonl")
    @info "Creating $manifestpath"
    open(manifestpath, "a") do f
        writemanifest(f, recordings)
    end

    manifestpath = joinpath(output_dir, "annotations.jsonl")
    @info "Creating $manifestpath"
    open(manifestpath, "a") do f
        writemanifest(f, annotations)
    end

end