using CSV, Unicode


function prepare(::Val{:PFC_LISN}, data_dir, output_dir)
    # Validate the data directory
    ! isdir(data_dir) && throw(ArgumentError("invalid path $(data_dir)"))

    # Create the output directory.
    output_dir = mkpath(output_dir)
    rm(joinpath(output_dir, "recordings.jsonl"), force=true)
    rm(joinpath(output_dir, "annotations.jsonl"), force=true)

    metadata = CSV.File(joinpath(pkgdir(@__MODULE__), "src", "corpora", "metadata", "pfc_metadata.csv"))

    annotations = Dict()
    recordings = Dict()

    for file in metadata
        id = file.file
       
        audio_src = AudioSources.FileAudioSource(joinpath(data_dir, "wav", Unicode.normalize(file.region), file.speaker, string(file.file,".wav")))

        recordings[id] = Recording(
            id,
            audio_src;
            channels = [1],
            samplerate = 1600
        )
    end

    manifestpath = joinpath(output_dir, "recordings.jsonl")
    @info "Creating $manifestpath"
    open(manifestpath, "a") do f
        writemanifest(f, recordings)
    end

    for file in metadata
        id = file.file

        file_annotations = CSV.File(joinpath(data_dir, "36CIf61ter", "lbl", string(id, ".lblmore")),  header=["start", "dur", "phoneme", "word", "context", "file"])

        annotations[id] = Annotation(
            id,
            id,
            -1,
            -1,
            [1],
            Dict(
                "segmentation" => file_annotations,
                "gender" => file.gender,
                "age_cat" => file.age_cat,
                "region" => file.region,
                "subset" => file.subset)            
        )      

    end

    manifestpath = joinpath(output_dir, "annotations.jsonl")
    @info "Creating $manifestpath"
    open(manifestpath, "a") do f
        writemanifest(f, annotations)
    end


end