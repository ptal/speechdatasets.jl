# SPDX-License-Identifier: CECILL-C

"""
    abstract type ManifestItem end

Base class for all manifest item. Every manifest item should have an
`id` attribute.
"""
abstract type ManifestItem end

"""
    struct Recording{Ts<:AbstractAudioSource} <: ManifestItem
        id::AbstractString
        source::Ts
        channels::Vector{Int}
        samplerate::Int
    end

A recording is an audio source associated with and id.

# Constructors
    Recording(id, source, channels, samplerate)
    Recording(id, source[; channels = missing, samplerate = missing])

If the channels or the sample rate are not provided then they will be
read from `source`.

!!! warning
    When preparing large corpus, not providing the channels and/or the
    sample rate can drastically reduce the speed as it forces to read
    source.
"""
struct Recording{Ts<:AudioSources.AbstractAudioSource} <: ManifestItem
    id::AbstractString
    source::Ts
    channels::Vector{Int}
    samplerate::Int
end

function Recording(uttid, s::AudioSources.AbstractAudioSource; channels = missing, samplerate = missing)
    if ismissing(channels) || ismissing(samplerate)
        x, sr = loadaudio(s)
        samplerate = ismissing(samplerate) ? Int(sr) : samplerate
        channels = ismissing(channels) ? collect(1:size(x,2)) : channels
    end
    Recording(uttid, s, channels, samplerate)
end

"""
    struct Annotation <: ManifestItem
        id::AbstractString
        recording_id::AbstractString
        start::Float64
        duration::Float64
        channel::Union{Vector, Colon}
        data::Dict
    end

An "annotation" defines a segment of a recording on a single channel.
The `data` field is an arbitrary dictionary holdin the nature of the
annotation. `start` and `duration` (in seconds) defines,
where the segment is locatated within the recoding `recording_id`.

# Constructor

    Annotation(id, recording_id, start, duration, channel, data)
    Annotation(id, recording_id[; channel = missing, start = -1, duration = -1, data = missing)

If `start` and/or `duration` are negative, the segment is considered to
be the whole sequence length of the recording.
"""
struct Annotation <: ManifestItem
    id::AbstractString
    recording_id::AbstractString
    start::Float64
    duration::Float64
    channels::Union{Vector, Colon}
    data::Dict
end

Annotation(id, recid; channels = missing, start = -1, duration = -1, data = missing) =
    Annotation(id, recid, start, duration, channels, data)


"""
    load(recording::Recording [; start = -1, duration = -1, channels = recording.channels])
    load(recording, annotation)

Load the signal from a recording. `start`, `duration` (in seconds)

The function returns a tuple `(x, sr)` where `x` is a ``N×C`` array
- ``N`` is the length of the signal and ``C`` is the number of channels
- and `sr` is the sampling rate of the signal.
"""
function AudioSources.load(r::Recording; start = -1, duration = -1, channels = r.channels)
    if start >= 0 && duration >= 0
        s = Int(floor(start * r.samplerate + 1))
        e = Int(ceil(duration * r.samplerate))
        subrange = (s:e)
    else
        subrange = (:)
    end

    AudioSources.load(r.source, subrange, channels)
end

"""
    load(r::Recording, a::Annotation)
    load(t::Tuple{Recording, Annotation})
Load only a segment of the recording referenced in the annotation.
"""
AudioSources.load(r::Recording, a::Annotation) = AudioSources.load(r; start = a.start, duration = a.duration, channels = a.channels)
AudioSources.load(t::Tuple{Recording, Annotation}) = AudioSources.load(t[1], t[2])

