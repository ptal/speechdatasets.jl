# SPDX-License-Identifier: CECILL-B


"""
    SpeechDataset

Store metadata about a speech dataset.
"""
struct SpeechDataset <: AbstractDict{String,Tuple{Recording,Union{Annotation,Missing}}}
    recordings::Dict{String, Recording}
    annotations::Dict{String, Annotation}

    name::String
    lang::Vector{String}
    license::String
    source::String
    authors::Vector{String}
    description::String
    subsets::Vector{String}
end


Base.length(d::SpeechDataset) = length(d.recordings)

function Base.iterate(d::SpeechDataset)
    retval = iterate(keys(d))
    isnothing(retval) ? nothing : (d[first(retval)], last(retval))
end

function Base.iterate(d::SpeechDataset, state)
    retval = iterate(keys(d), state)
    isnothing(retval) ? nothing : (d[first(retval)], last(retval))
end

Base.keys(d::SpeechDataset) = keys(d.recordings)
Base.values(d::SpeechDataset) = collect(d[k] for k in keys(d))
Base.getindex(d::SpeechDataset, key::String) = d.recordings[String(key)], get(d.annotations, String(key), missing)
Base.get(d::SpeechDataset, key, default) = key ∈ keys(d) ? d[key] : default

#struct SpeechDataset <: MLUtils.AbstractDataContainer
#    infos::SpeechDatasetInfos
#    idxs::Vector{AbstractString}
#    annotations::Dict{AbstractString, Annotation}
#    recordings::Dict{AbstractString, Recording}
#end



#Base.getindex(d::SpeechDataset, key::AbstractString) = recordings(d)[key], annotations(d)[key]
#Base.getindex(d::SpeechDataset, idx::Integer) = getindex(d, idxs(d)[idx])
# Fix1 -> partial function with fixed 1st argument
#Base.getindex(d::SpeechDataset, idxs::AbstractVector) = map(Base.Fix1(getindex, d), idxs)

#Base.length(d::SpeechDataset) = length(d.idxs)

#function Base.filter(fn, d::SpeechDataset)
#    fidxs = filter(d.idxs) do i
#        fn((d.recordings[i], d.annotations[i]))
#    end
#    idset = Set(fidxs)
#
#    fannotations = filter(d.annotations) do (k, v)
#        k ∈ idset
#    end
#
#    frecs = filter(d.recordings) do (k, v)
#        k ∈ idset
#    end
#
#    SpeechDataset(d.infos, fidxs, fannotations, frecs)
#end


"""
    dataset(dataset, inputdir::AbstractString, outputdir::AbstractString; kwargs...)

Create a [`SpeechDataset`](@ref) object for `dataset`. `inputdir` is the
directory containing the raw data. If the `inputdir` does not exist and the
data is freely available, it will be automatically downloaded and put in
`inputdir`. `outputdir` is the directory where will be stored summary files.
`kwargs`... are dataset specific arguments passed to `dataset`
"""
function dataset(name::Symbol, inputdir::AbstractString, outputdir::AbstractString; kwargs...)

    # download if possible and inputdir doesn't exists yet
    downloadable = hasmethod(download, Tuple{Val{name}, AbstractString})
    if downloadable && ! isdir(inputdir)
        download(Val(name), inputdir)
    end

    # prepare if done already
    if ! (isfile(joinpath(outputdir, "recordings.jsonl")) && isfile(joinpath(outputdir, "annotations.jsonl")))
        prepare(Val(name), inputdir, outputdir; kwargs...)
    end

    annotations = load_manifest(
        Annotation,
        joinpath(outputdir, "annotations.jsonl")
    )
    recordings = load_manifest(
        Recording,
        joinpath(outputdir, "recordings.jsonl")
    )

    meta = _METADATA[String(name)]
    SpeechDataset(
        recordings,
        annotations,
        meta["name"],
        meta["lang"],
        meta["license"],
        meta["source"],
        meta["authors"],
        meta["description"],
        meta["subsets"],
    )
end

