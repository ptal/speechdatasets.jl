# SPDX-License-Identifier: CECILL-C

module SpeechDatasets

using JSON
import AudioSources
using SpeechFeatures
import MLUtils
using DataFrames
using CSV

export
    # ManifestItem
    Recording,
    Annotation,
    load,

    # Manifest interface
    writemanifest,
    readmanifest,

    # Lexicon
    CMUDICT,
    TIMITDICT,
    MFAFRDICT,

    # Builder
    DatasetBuilder,
    get_kwargs,
    get_dataset_kwargs,
    get_nametype,
    download,
    prepare,

    # Dataset
    SpeechDatasetInfos,
    SpeechDataset,
    summary,
    dataset

const _METADATA = JSON.parsefile(
    joinpath(@__DIR__, "corpora", "corpora.json")
)

include("manifest_item.jl")
include("manifest_io.jl")
#include("builder.jl")
include("dataset.jl")

# Supported corpora
include("corpora/timit.jl")
include("corpora/pfc_lisn.jl")
include("corpora/faetar_asr_challenge_2025.jl")
include("corpora/mini_librispeech.jl")
include("corpora/synthetic_vowel_dataset.jl")

#include.("corpora/".*filter(contains(r"\.jl$"), readdir(joinpath(@__DIR__, "corpora"))))
include("lexicons.jl")

# declare all supported builders
#declareBuilder.(get_nametype.(corpora_names))

end
