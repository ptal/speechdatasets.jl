# SpeechDatasets.jl
Convenient and unified way to load a speech dataset. It can then be harnessed with other PTAL tools.  

A `SpeechDataset` instance consists of a set of recordings (info about audio data) and annotations.

## Contents 

```@contents
Pages = ["index.md", "installation.md", "examples.md", "api.md", "datasets.md", "newdataset.md"]
```

## License
This software is provided under the [CeCILL-C license](https://cecill.info/licences.en.html)

## Authors

- Lucas Ondel Yang
- Nicolas Denier
- Simon Devauchelle

![](https://ptal.lisn.upsaclay.fr/assets/lisn-ups-cnrs.png)