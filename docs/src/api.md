# API

## Load a Dataset
To get data from a supported dataset, you only need one function:
```@docs
dataset

```

## Types
### SpeechDataset
```@docs
SpeechDataset
```
Access a single element with integer or id indexing
```julia
# ds::SpeechDataset
ds[1]
ds["1988-147956-0027"]
```

### Manifest items
```@docs
SpeechDatasets.ManifestItem
Recording
Annotation
AudioSources.load(r::Recording; start = -1, duration = -1, channels = r.channels)
AudioSources.load(r::Recording, a::Annotation)
SpeechDatasets.load_manifest(T::Type{<:Union{Recording, Annotation}}, path)
```

## Lexicons
```@docs
CMUDICT(path)
TIMITDICT(timitdir)
MFAFRDICT(path)
```

## Index

```@index
```
