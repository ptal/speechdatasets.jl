push!(LOAD_PATH,"..")

using Documenter, SpeechDatasets, AudioSources
using Documenter.Remotes

include("deployconfig.jl")

makedocs(
    sitename="SpeechDatasets",
    repo = Remotes.GitLab("gitlab.lisn.upsaclay.fr", "PTAL", "Datasets/SpeechDatasets.jl"),
    doctest = false,
    pages = [
        "Home" => "index.md",
        "Installation" => "installation.md",
        "Examples" => "examples.md",
        "API" => "api.md",
        #= "Supported datasets" => "datasets.md", OUTDATED =#
        #= "Add a new dataset" => "newdataset.md", OUTDATED =#
    ]
)

config = GitLabHTTPS()

deploydocs(
    repo = "gitlab.lisn.upsaclay.fr/PTAL/Datasets/SpeechDatasets.jl",
    branch = "docs",
    deploy_config = GitLabHTTPS(),
    versions=["latest"=>"v^", "v#.#.#"]
)
