# use custom image stored in GitLab Container Registry
image: ${CI_REGISTRY}/ptal/documentation/buildimage:latest
stages:
  - test
  - build-docs
  - check
  - deploy
  - build-badges
  - deploy-badges

# [RULE] Rule to run a job only on merge request on main (default branch can have another name than main)
.only-on-merge-request:
  rules:
    - if: $CI_MERGE_REQUEST_TARGET_BRANCH_NAME == $CI_DEFAULT_BRANCH

# [RULE] Run a job only when a version tag is pushed on main branch
.only-vtag:
  rules:
    - if: $CI_COMMIT_TAG =~ /^v\d+.\d+.\d+-?.*$/

# [RULE] Run a job only after a merge on main branch
.only-after-merge:
 rules:
    - if: '$CI_PIPELINE_SOURCE == "push" && $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'

# [DEPLOY] Get Website docs and docs branch for a given project
.clone-website-docs: &clone-website-docs
  - git config --global user.email "$GITLAB_USER_EMAIL"
  - git config --global user.name "$GITLAB_USER_NAME"
  # get website files
  - git clone -b main --single-branch https://website-token:${WEBSITE_TOKEN}@${CI_SERVER_HOST}/PTAL/Website.git
  # get docs branch of current project
  - git clone -b docs --single-branch "$CI_REPOSITORY_URL" 
  
# [DEPLOY] Establish ssh connexion
.ssh-connect: &ssh-connect
  - mkdir -p ~/.ssh
  - chmod 0700 ~/.ssh
  - echo "$KNOWN_HOST" | base64 -d >> ~/.ssh/known_hosts
  - chmod 600 ~/.ssh/known_hosts
  - eval `ssh-agent -s`
  - echo "$DEPLOY_KEY" | base64 -d | ssh-add -

# [DEPLOY] Push changes to Website
.website-push: &website-push
  - cd Website
  - |
    # if uncommitted changes
    if [ -n "$(git status --porcelain)" ]; then 
      git add -A
      git commit -m "update docs for $CI_PROJECT_NAME $LABEL"
      git push origin
      # sync changes with remote server (only project dir), sync also deletions
      rsync -Rrl --delete ptal/./${CI_PROJECT_NAME}/ ${DEPLOY_DESTINATION}
    else 
      echo "No changes to docs"
    fi
  # cleaning
  - cd .. # important to have the correct .status
  - rm -r Website $CI_PROJECT_NAME

# Add badges pipeline
include:
  - local: '.gitlab-ci-badges.yml'


# Run unit tests if provided on merge request 
tests:
  stage: test
  variables:
    PREFIX: "TEST" # badge prefix
  extends: 
    - .dotenv # share job status in .env
    - .only-on-merge-request
  before_script:
   - eval "$FAILED" # set status to failed by default
   - |
     julia -e '
      using Pkg
      Pkg.activate(; temp = true)
      Pkg.resolve()
      Pkg.precompile()'
  script:
    - |
      if [ -f test/runtests.jl ]; then
        julia --project=./ -e 'using Pkg; Pkg.test()'
        eval "$PASSED"
      else
        echo "[warning] no tests provided"
        eval "$NONE"
      fi


# Build documentation on merge request
build-docs:
  stage: build-docs    
  extends: 
    - .dotenv # share job status in .env
    - .only-on-merge-request
  variables:
    PREFIX: "BUILD_DOCS" # badge prefix
  before_script:
    - eval "$FAILED" # set status to failed by default
    - |
      julia --project=docs -e '
        using Pkg
        Pkg.develop(PackageSpec(path=pwd()))
        Pkg.instantiate()'
  script:
    - julia --project=docs docs/make.jl
    - eval "$PASSED" # set status to passed


# Ensure version references are consistent
check-version:
  stage: check
  variables:
    PREFIX: "DEPLOY" # badge prefix (this one is not directly a badge but a condition for other deploy badges)
  extends: 
    - .dotenv # share job status in .env
    - .only-vtag
  before_script:
    - eval "$FAILED" # set status to failed by default
  script:
    # check that the tag is on main branch
    - LATEST_COMMIT_MAIN=$(curl -s "https://gitlab-ci-token:${CI_JOB_TOKEN}@${CI_SERVER_HOST}/api/v4/projects/${CI_PROJECT_ID}/repository/commits/${CI_DEFAULT_BRANCH}" | jq -r .id)
    - if [[ $LATEST_COMMIT_MAIN != $CI_COMMIT_SHA ]]; then echo "Tagging and deploying a version is allowed only on the HEAD of ${CI_DEFAULT_BRANCH}" >&2; exit 1; fi
    # check coherence between project version and version tag
    - PROJECTVERSION=$(cat Project.toml | grep "version" | grep -o '[0-9]*\.[0-9]*\.[0-9]*')
    - if ! [[ $CI_COMMIT_TAG =~ $PROJECTVERSION ]]; then
    -   echo "Project version ($PROJECTVERSION) and version tag ($CI_COMMIT_TAG) mismatch" >&2; exit 1;
    - fi
    # check coherence between version tag and registered versions
    - RELEASES=$(curl -s ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/releases/)
    - if [[ "$(echo $RELEASES | jq length)" -ne 0 ]]; then # only if it is not a new project
    -   for VERSION in $(echo $RELEASES | jq -r '.[].tag_name'); do
    -     if [[ $VERSION == "$CI_COMMIT_TAG" ]]; then 
    -       echo "Version $CI_COMMIT_TAG is already deployed" >&2; exit 1; 
    -     fi 
    -   done
    - fi
    - eval "$PASSED" # set status to passed


# Register a new version on new version tag
deploy-version:
  stage: deploy
  variables:
    PREFIX: "DEPLOY_VERSION" # badge prefix
  extends: 
    - .dotenv # share job status in .env
    - .only-vtag
  before_script:
    - eval "$FAILED" # set status to failed by default    
    - git config --global user.email "$GITLAB_USER_EMAIL"
    - git config --global user.name "$GITLAB_USER_NAME"
    - julia -e 'using Pkg; pkg"registry rm \"PTAL\""' # remove PTAL registry and then add it with auth token (allowing to push new package)
    - julia -e "using Pkg; Pkg.add(\"LocalRegistry\"); pkg\"registry add https://registry-token:${REGISTRY_TOKEN}@${CI_SERVER_HOST}/PTAL/Registry.git\""
  script:
    - |
      julia --project=. -e '
        using Pkg, LocalRegistry
        Pkg.Registry.update()
        register(; registry = "PTAL", repo="$(ENV["CI_PROJECT_URL"]).git")' 
    - eval "$PASSED" # set status to passed
  release:
    tag_name: '$CI_COMMIT_TAG'
    description: '$CI_COMMIT_TAG'


# Deploy dev documentation after merging to main
deploy-dev-docs:
  stage: deploy
  variables:
    DEV: dev # name for development docs
    LABEL: $DEV # used in Website commit message
  extends: 
    - .only-after-merge
  before_script: 
    # skip if no docs 
    - |
      if ! (git ls-remote --heads $CI_REPOSITORY_URL docs | grep -q docs); then
        echo "No branch docs, skipped docs deployment."
        exit 0
      fi
    - *clone-website-docs
    # copy docs to website
    # exclude version directories (prevent overriding previous doc)
    - mkdir -p Website/ptal/${CI_PROJECT_NAME}
    - echo -e "exclude.txt\n.git\nlatest" > exclude.txt
    - cd ${CI_PROJECT_NAME}
    - LOCALVERSIONS=$(ls | grep -E 'v[0-9]+\.[0-9]+\.[0-9]+')
    - echo "$LOCALVERSIONS" >> ../exclude.txt || true # does not fail if empty
    - REMOTEVERSIONS=$(curl -s ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/releases/ | jq -r '.[].tag_name')
    # get intersection between local versions (docs available) and remote versions (releases)
    - VERSIONS=$(echo -e "$REMOTEVERSIONS\n$LOCALVERSIONS" | sort | uniq -Du)
    # overwrite versions.js
    - |
      echo "var DOC_VERSIONS = [
      \"$DEV\",
      ${VERSIONS:+\"latest\",}
      $(sed -e 's/\(.*\)/\"\1\",/' <<< $VERSIONS)
      ];
      ${VERSIONS:+var DOCUMENTER_NEWEST = \"$(head -n 1 <<< $REMOTEVERSIONS)\";}
      ${VERSIONS:+var DOCUMENTER_STABLE = \"latest\";}
      " > versions.js
    # copy latest docs to dev/ 
    - mkdir -p $DEV
    - cp -rL latest/* $DEV
    - sed -i "s/\".*\"/\"$DEV\"/" $DEV/siteinfo.js
    - cd ..
    # update Website docs locally
    - rsync -rl --exclude-from='exclude.txt' ${CI_PROJECT_NAME}/ Website/ptal/${CI_PROJECT_NAME}/
    # establish ssh connexion
    - *ssh-connect
  script:
    # push changes to Website
    - *website-push


# Deploy documentation once build-docs succeeded on new version tag
deploy-docs:
  stage: deploy
  variables:
    PREFIX: "DEPLOY_DOCS" # badge prefix
    LABEL: $CI_COMMIT_TAG # used in Website commit message
  extends: 
    - .dotenv # share job status in .env
    - .only-vtag
  before_script: 
    - eval "$FAILED" # set status to failed by default
    - *clone-website-docs
    # copy docs to website
    # exclude directories that are not the last version (prevent overriding previous doc)
    - echo 'exclude.txt' > exclude.txt
    - echo '.git' > exclude.txt
    - (cd ${CI_PROJECT_NAME} && ls | grep -E 'v[0-9]+\.[0-9]+\.[0-9]+') | grep -v "$CI_COMMIT_TAG" >> exclude.txt || true # does not fail if empty
    - mkdir -p Website/ptal/${CI_PROJECT_NAME}
    - rsync -rl --exclude-from='exclude.txt' ${CI_PROJECT_NAME}/ Website/ptal/${CI_PROJECT_NAME}/
    # establish ssh connexion
    - *ssh-connect
  script:
    # push changes to Website
    - *website-push
    - eval "$PASSED" # set status to passed